# Taxi Triplog Encryption Utility

A simple javascript program that does the following:

1. Once started, it tracks file additions or changes to a predefined input directory (default: "files/input/")
2. When a file is added, it encrypts the DRIVERID field using a predefined password (default: "abc123")
3. Compresses the encrypted file and saves it to a predefined output directory (default: "files/output/")

## upgrading to 1.0.1.*

1. override the exsiting config file with one downloaded on version 1.0.1.*
2. insert the aws credentials (aws_access_key and aws_secret_key)

### Note that you can control 
1. the exported fields (per file type : trip/shift)
2. the encrypted/encoded fields (cross file type) 

## Installation

### Install encryption software

1. Install node.js https://nodejs.org/en/download/ (Click your Operating System logo to download)
3. Change dir ```cd triplog_encryptor```
4. Change the default password in ```config.json```
5. Install and run ```npm install & npm start```

### Upload to Streetsmart

1. copy credentials to config.json
2. change the access and secrect keys and save

Triplog shifts and trips CSV files that will be added to files/input dir will be encrypted to files/output dir.

## Decrypting an encrypted value

This script also allows you to decrypt a given text:
```node src/decrypt.js YOUR_ENCRYPTED_VALUE_HERE```

The decrypted value will appear in the console.

## Run as a service

In order to run the triplog encryptor as a service :

1. Download NSSM https://nssm.cc/release/nssm-2.24.zip
2. Follow installation instructions at https://nssm.cc/usage
3. configure as follows:
    - Path : C:\Program Files\nodejs\node.exe
    - Startup Directory : <the directory where the triplog encryptor software is located>
    - Arguments : <Startup directory>\src\app.js
    - Service Name : TriplogEncryptor
4. you can configure logon details if needed on the logon tab   





