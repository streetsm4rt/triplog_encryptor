// Nodejs encryption with CTR
var mkdirp = require('mkdirp');
var crypto = require('crypto');
var CRYPTO_ALGO = 'aes-256-ctr';
var csv = require('fast-csv');
var fs = require('fs');
var path = require('path');
var zlib = require('zlib');
var chokidar = require('chokidar');
var log = console.log.bind(console);
var AWS = require('aws-sdk'); 
var path = require('path');

//Constructs a encryptor
function Encryptor(settings) {
    this.config = JSON.parse(JSON.stringify(settings));
    //no fallback of the handled columns
    //this.config.encode_columns = settings.encode_columns|| ["EHailId"],
    //this.config.encrypt_columns = settings.encrypt_columns|| ["DRIVERID","Cabnumber"]
    this.config.input_dir = settings.input_dir || "files/input";
    this.config.tmp_dir = settings.tmp_dir || "files/tmp";
    this.config.output_dir = settings.output_dir || "files/output";
    this.config.password = settings.password || 'abc123';
    this.config.aws_access_key = settings.aws_access_key || "example";
    this.config.aws_secret_key = settings.aws_secret_key || "example"; 
    if(this.config.aws_access_key == "example" || this.config.aws_secret_key=="example")
        console.warn("aws credentials are not initialized, please go to ./config.json and type valid aws credentials");
    
    return this;
};

Encryptor.prototype.getConfig=function (){
  return this.config;
}

//Encrypts a given csv file, place the result to the outputfile
Encryptor.prototype.encryptFile = function(inputFile,  outputFile)
{
    try
    {
        if(path.extname(inputFile) != ".csv"){
            console.warn("no supported file extension detected : " + inputFile);
            return;
        }

        var tmpFile = inputFile.replace(this.config.input_dir,this.config.tmp_dir)+".gz";
      
        if(!outputFile)
        {
             outputFile = inputFile.replace(this.config.input_dir,this.config.output_dir)+".gz";
            
            if(fs.existsSync(outputFile))
            {
                log("skipping", inputFile, ">>", outputFile, "(already exists)");
                return;
            }
        }
        var _this = this;
        var exists_columns = this.config.encode_columns;
        var encrypt_columns = this.config.encrypt_columns;

        var gzip = zlib.createGzip();
      
        var op = inputFile + " >> " + outputFile;
        console.time(op);
        
        var ws = fs.createWriteStream(tmpFile, {encoding: "utf8"});

       
       get_line(inputFile, 1, function(stream, err, line){
           log("line == " + line);
       
            var fileType;
        
            if(line.toLowerCase().indexOf("trip_detailid") !== -1)
                fileType = "trip";
            else    
                fileType = "shift";
         
        
            var k = keep_columns(_this.config, fileType);
            log("keep_columns : " + JSON.stringify(k));
            var csvWriteStream =   csv.createWriteStream({headers: k, quoteColumns: true});

            csv
            .fromPath(inputFile, {  discardUnmappedColumns: true, headers: true })
            .transform(function(obj){
                //foreach row in the csv 
                try{    
                    //anonymized fields - encrypt the fields
                    encrypt_columns.forEach(function (prop) {
                        log("enrypted_columns " + prop);
                        if(obj[prop])
                            obj[prop] = _this.encryptText(obj[prop]);
                    });
                    exists_columns.forEach(function(prop){
                        log("encode_columns " + prop);
                        obj[prop] = (obj[prop])  && (obj[prop] != "") && (obj[prop] !="NaN") ? 1 : 0;
                    });                
                }
                catch(eTrans){
                    log ("error during processing file : ",op, "Details : " +  eTrans);
                }
                return obj;
            })
            .pipe(csvWriteStream)
            .pipe(gzip)
            .pipe(ws)
            .on('finish', function () {  // finished
                fs.rename(tmpFile, outputFile,function(err){
                    if(err){
                        console.log("error while renaming : " + err);
                    }
                    else { 
                        try{
                            AWS.config.update({
                                region: "us-east-1",
                                accessKeyId: _this.config.aws_access_key,
                                secretAccessKey: _this.config.aws_secret_key,
                                correctClockSkew : true
                            });

                            var s3 = new AWS.S3(); 
                            
                            //replace back-slashes with fwd slashes to support windows filepath
                            var find = '\\\\';
                            var re = new RegExp(find, 'g');
                            outputFile = outputFile.replace(re, '/');

                            var body = fs.createReadStream(outputFile);
                            var params = {
                                Bucket: 'warehouse.streetsm4rt.com',
                                Key: 'extract/verifone/alltaxi/raw/' + outputFile,
                                Body: body,
                            };
                        
                            s3.putObject(params, function(err, data) {
                                if (err) {
                                    console.log("error while uploading result to s3. file " + outputFile);
                                    console.log(err, err.stack); // an error occurred
                                } else  {
                                    console.log("file - " + inputFile + " >>> uploaded successfully");    
                                    console.timeEnd(op);
                                    console.log("finished working with " + inputFile);
                                }
                            
                            });
                        }catch(ex){
                            console.log("exception while uploading result to s3. file " + outputFile);
                        }
                    }
                });
            });
        });
    }
   catch(eOp)
   {
      log("error during processing file : ", op,"Details : " + eOp);
   }   
}


function get_line(filename, line_no, callback) {
    var stream = fs.createReadStream(filename, {
      flags: 'r',
      encoding: 'utf-8',
      fd: null,
      mode: 0666,
      bufferSize: 64 * 1024
    });

    var fileData = '';
    stream.on('data', function(data){
      fileData += data;

      // The next lines should be improved
      var lines = fileData.split("\n");

      if(lines.length >= +line_no){
        stream.destroy();
        callback(stream,null, lines[0]);
      }
    });

    stream.on('error', function(){
      callback(stream, 'Error', null);
    });

    stream.on('end', function(){
      callback(stream, 'File end reached without finding line', null);
    });

}

function keep_columns(config, fileType){
    if(fileType == "shift"){
        return calc_keep_columns(config.shift_all_columns, config.shift_remove_columns);
    } else if (fileType == "trip"){
        return calc_keep_columns(config.trip_all_columns, config.trip_remove_columns);
    } else
        return [];
}

function calc_keep_columns(all_columns, remove_columns){
        var keep_columns = [];
        
        all_columns.forEach(function(column){
            if(!(remove_columns.indexOf(column) >= 0))
                keep_columns.push(column);
        });

        return keep_columns;
}
//encrypt a given text
Encryptor.prototype.encryptText=function (text){
  var cipher = crypto.createCipher(CRYPTO_ALGO,this.config.password);
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
//decrypt a given text 
Encryptor.prototype.decryptText = function(text){
  var decipher = crypto.createDecipher(CRYPTO_ALGO,this.config.password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

//watch the given directory (the default is ./files/input)
//trigger encrypt file when file is added
Encryptor.prototype.watchDir = function(){
   
   var _this = this;
    if(!this.watcher)
    {
        //ensure directories exist
        mkdirp(this.config.input_dir , function (err) {
            if (err) console.error(err);
        });
        mkdirp(this.config.tmp_dir , function (err) {
            if (err) console.error(err);
        });
        mkdirp(this.config.output_dir , function (err) {
            if (err) console.error(err);
        });

        //log the config
        var confClone = JSON.parse(JSON.stringify(this.config))
        confClone.password = "***";
        log("Encryptor running, config: ", JSON.stringify(confClone));
    
        this.watcher = chokidar.watch(this.config.input_dir, {
            ignored: /[\/\\]\./, persistent: true
            });
        
         this.watcher
            .on('add', function(path) 
            { 
                // file was added -> trigger file encrypt
                _this.encryptFile(path);
                log('File', path, 'has been added');

            })
             .on('change', function(path) { log('File', path, 'has been changed'); })
     }
}
 
module.exports = Encryptor;